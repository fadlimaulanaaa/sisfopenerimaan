var jurusanApp = angular.module('jurusanApp', []);
jurusanApp.controller("JurusanController", ['$scope', '$http', function($scope, $http) {
    getJurusan();

    function getJurusan() {
        $http.post('library/detailJurusan.php').then((data) => {
            console.log(data);
            $scope.details = data;
        });
    }
    $scope.show_form = true;
    $scope.formToggle = function() {
        $('#jurusanForm').slideToggle();
        $('#editJurusan').css('display', 'none');
    }
    $scope.insertJurusan = function(info) {
        $http.post('library/insertJurusan.php', { "jurusan": info.jurusan }).then(function(data) {
            if (data.data == true) {
                swal("Good job!", "You clicked the button!", "success");
                getJurusan();
            }
        });
    }
    $scope.deleteJurusan = function(info) {
        $http.post('library/deleteJurusan.php', { "id_jurusan": info.id_jurusan }).then(function(data) {
            if (data.data == true) {
                swal("Good job!", "You clicked the button!", "success");
                getJurusan();
            }
        });

    }
    $scope.currentJurusan = {};
    $scope.editJurusan = function(info) {
        $scope.currentJurusan = info;
        $('#jurusanForm').slideUp();
        $('#editJurusan').slideToggle();
    }

    $scope.updateJurusan = function(info) {
        $http.post('library/updateJurusan.php', {
            "id_jurusan": info.id_jurusan,
            "jurusan": info.nama_jurusan
        }).then(function(data) {
            $scope.show_form = true;
            if (data.data == true) {
                swal("Good job!", "You clicked the button!", "success");
                getJurusan();
            }
        });
    }

    $scope.updateMsg = function(id_jurusan) {
        console.log(info)
    }

    $scope.tambahJurusan = function() {
        var myModal = new bootstrap.Modal(document.getElementById('tambah'), {
            keyboard: false
        })
        myModal.show();
    }

    $scope.editJurusan = function(detail) {
        var myModal = new bootstrap.Modal(document.getElementById('edit'), {
            keyboard: false
        })
        $scope.currentJurusan = detail;
        myModal.show();
    }
}]);