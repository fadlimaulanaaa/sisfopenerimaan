var mahasiswaApp = angular.module('mahasiswaApp', []);
mahasiswaApp.controller("DbController", ['$scope', '$http', function($scope, $http) {

    getMahasiswa();

    function getMahasiswa() {
        $http.post('library/detailMahasiswa.php').success(function(data) {
            $scope.details = data;
        });
    }
    $scope.mhsInfo = { 'jenis_kelamin': 'laki-laki' };

    $scope.show_form = true;
    $scope.formToggle = function() {
        $('#mahasiswaForm').slideToggle();
        $('#editMahasiswa').css('display', 'none');
    }

    $scope.insertMahasiswa = function(info) {
        $http.post('library/insertMahasiswa.php', {
            "nim": info.nim,
            "nama_lengkap": info.nama_lengkap,
            "nama_panggilan": info.nama_panggilan,
            "alamat": info.alamat,
            "email": info.email,
            "tempat_lahir": info.tempat_lahir,
            "tgl_lahir": info.tgl_lahir,
            "agama": info.agama,
            "jenis_kelamin": info.jenis_kelamin,
            "jurusan": info.jurusan
        }).success(function(data) {
            if (data == true) {
                getMahasiswa();
                $('#mahasiswaForm').css('display', 'none');

            }
        });
    }

    $scope.deleteMahasiswa = function(info) {
        $http.post('library/deleteMahasiswa.php', {
            "id_mahasiswa": info.id_mahasiswa
        }).success(function(data) {
            if (data == true) {
                getMahasiswa();
            }
        });
    }

    $scope.currentMahasiswa = {};
    $scope.editMahasiswa = function(info) {
        $scope.currentMahasiswa = info;
        $('#mahasiswaForm').slideUp();
        $('#editMahasiswa').slideToggle();
    }

    $scope.updateMahasiswa = function(data) {
        $http.post('library/updateMahasiswa.php', {
            "id_mahasiswa": info.id_mahasiswa,
            "nim": info.nim,
            "nama_lengkap": info.nama_lengkap,
            "nama_panggilan": info.nama_panggilan,
            "alamat": info.alamat,
            "email": info.email,
            "tempat_lahir": info.tempat_lahir,
            "tgl_lahir": info.tgl_lahir,
            "agama": info.agama,
            "jenis_kelamin": info.jenis_kelamin,
            "jurusan": info.jurusan
        }).success(function(data) {
            $scope.show_form = true;
            if (data == true) {
                getMahasiswa
            }
        });
    }

    $scope.updateMsg = function(id_mahasiswa) {
        $('#editMahasiswa').css('display', 'none');
    }

    getJurusan();

    function getJurusan() {
        $http.post('library/detailJurusan.php').success(function(data) {
            $scope.kampus = data;
        });
    }

}]);